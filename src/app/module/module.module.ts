import { NgModule } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
const materialModulecomponent = [
  MatButtonModule,
  MatButtonToggleModule,
  MatIconModule,
  MatTableModule
];
@NgModule({
  imports: [materialModulecomponent],
  exports: [materialModulecomponent]
})
export class ModuleModule { }
