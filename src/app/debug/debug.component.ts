import { DebugService } from './../debug.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-debug',
  templateUrl: './debug.component.html',
  styleUrls: ['./debug.component.css'],
  providers: [DebugService]
})
export class DebugComponent implements OnInit {

  constructor(private debugService: DebugService) { }

  ngOnInit(): void {
    this.debugService.info('Debug component gets service from Parent');
  }

}
