import { EditentryComponent } from './editentry/editentry.component';

import { AboutComponent } from './about/about.component';

import { ExpenseGuard } from './expense.guard';
import { LogoutComponent } from './logout/logout.component';
import { LoginComponent } from './login/login.component';

import { ExpenseEntryListComponent } from './expense-entry-list/expense-entry-list.component';
import { ExpenseEntryComponent } from './expense-entry/expense-entry.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: 'about', component: AboutComponent },
   { path: 'login', component: LoginComponent },
   { path: 'logout', component: LogoutComponent },
   { path: 'expenses', component: ExpenseEntryListComponent, canActivate: [ExpenseGuard]},
   { path: 'expenses/detail/:id', component: ExpenseEntryComponent, canActivate: [ExpenseGuard]},
   { path: 'expenses/add', component:EditentryComponent , canActivate: [ExpenseGuard]},
   { path: 'expenses/edit/:id', component: EditentryComponent , canActivate: [ExpenseGuard]},
   { path: '', redirectTo: 'expenses', pathMatch: 'full' }
];
@NgModule({
   imports: [RouterModule.forRoot(routes)],
   exports: [RouterModule] })
export class AppRoutingModule { }
